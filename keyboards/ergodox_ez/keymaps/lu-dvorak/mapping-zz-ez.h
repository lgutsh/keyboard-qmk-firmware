#pragma once

#define  LU_ZZ_EZ_1 KC_1
#define  LU_ZZ_EZ_2 KC_2
#define  LU_ZZ_EZ_3 KC_3
#define  LU_ZZ_EZ_4 KC_4
#define  LU_ZZ_EZ_5 KC_5
#define  LU_ZZ_EZ_6 KC_6
#define  LU_ZZ_EZ_7 KC_7
#define  LU_ZZ_EZ_8 KC_8
#define  LU_ZZ_EZ_9 KC_9
#define  LU_ZZ_EZ_0 KC_0

#define  LU_ZZ_EZ_SEMICOLON  KC_Q
#define  LU_ZZ_EZ_COMMA      KC_W
#define  LU_ZZ_EZ_DOT        KC_E
#define  LU_ZZ_EZ_P          KC_R
#define  LU_ZZ_EZ_Y          KC_T
#define  LU_ZZ_EZ_F          KC_Y
#define  LU_ZZ_EZ_G          KC_U
#define  LU_ZZ_EZ_C          KC_I
#define  LU_ZZ_EZ_R          KC_O
#define  LU_ZZ_EZ_L          KC_P
#define  LU_ZZ_EZ_A          KC_A
#define  LU_ZZ_EZ_O          KC_S
#define  LU_ZZ_EZ_E          KC_D
#define  LU_ZZ_EZ_U          KC_F
#define  LU_ZZ_EZ_I          KC_G
#define  LU_ZZ_EZ_D          KC_H
#define  LU_ZZ_EZ_H          KC_J
#define  LU_ZZ_EZ_T          KC_K
#define  LU_ZZ_EZ_N          KC_L
#define  LU_ZZ_EZ_S          KC_SCLN
#define  LU_ZZ_EZ_QUOTES     KC_Z
#define  LU_ZZ_EZ_Q          KC_X
#define  LU_ZZ_EZ_J          KC_C
#define  LU_ZZ_EZ_K          KC_V
#define  LU_ZZ_EZ_X          KC_B
#define  LU_ZZ_EZ_B          KC_N
#define  LU_ZZ_EZ_M          KC_M
#define  LU_ZZ_EZ_W          KC_COMM
#define  LU_ZZ_EZ_V          KC_DOT
#define  LU_ZZ_EZ_Z          KC_SLSH

// Symbols
#define  LU_ZZ_EZ_AMPERSAND_AT          KC_GRAVE
#define  LU_ZZ_EZ_MINUS_UNDERSCORE      KC_QUOT
#define  LU_ZZ_EZ_SLASH_BACKSLASH       KC_LBRC
#define  LU_ZZ_EZ_ASTERISK_PLUS         KC_MINS
#define  LU_ZZ_EZ_PERCENT_EQUALS        KC_NUHS
#define  LU_ZZ_EZ_QUESTION_EXCLAMATION  KC_RBRC
#define  LU_ZZ_EZ_TILDE_GRAVE           KC_EQL

// Shift level 1, 3 and 5
#define  LU_ZZ_EZ_LSHIFT KC_LSHIFT
#define  LU_ZZ_EZ_RSHIFT KC_RSHIFT
#define  LU_ZZ_EZ_LTOP KC_ALGR
#define  LU_ZZ_EZ_RTOP KC_ALGR
#define  LU_ZZ_EZ_LFRONT KC_NUBS
#define  LU_ZZ_EZ_RFRONT KC_NUBS

// Control, alt and super
#define LU_ZZ_EZ_LCTRL KC_LCTL
#define LU_ZZ_EZ_RCTRL KC_RCTL
#define LU_ZZ_EZ_LALT KC_LALT
#define LU_ZZ_EZ_RALT KC_LALT
#define LU_ZZ_EZ_LSUPER KC_LWIN
#define LU_ZZ_EZ_RSUPER KC_RWIN

// Lock keys
#define LU_ZZ_EZ_LNUMLOCK KC_NLCK
#define LU_ZZ_EZ_RNUMLOCK KC_NLCK
#define LU_ZZ_EZ_LSCROLLLOCK KC_SLCK
#define LU_ZZ_EZ_RSCROLLLOCK KC_SLCK

// we have escape instead of caps
#define LU_ZZ_EZ_LESCAPE KC_CAPS
#define LU_ZZ_EZ_RESCAPE KC_CAPS

