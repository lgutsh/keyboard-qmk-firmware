#pragma once

typedef enum {
    // Standard dvorak US layout
    // Assuming that escape:swapcase is enabled
    DVORAK = 0,
    // Additional layer to have easier access to symbols based on mnemonics
    SYM,
    // Additional layer to have better access to parentheses, brackets and braces
    BRA,
    // Function keys, all navigation keys, insert and delete
    FUN,
    TRANSPARENT
} layers;
