#include QMK_KEYBOARD_H

#include "layer-names.h"

#include <quantum/keymap_extras/keymap_dvorak.h>
#include <ergodox_ez.h>

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
/* Keymap 0: Basic layer, slightly modified US DVORAK
 * 1) assuming that caps:swapescape is enabled
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * | `~     |   1! |   2@ |   3# |   4$ |   5% |   [{ |           |   ]} |   6^ |   7& |   8* |   9( |   0) | =+     |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * | TAB    |   '" |   ,< |   .> |   p  |   y  |   /? |           |   =+ |   f  |   g  |   c  |   r  |   l  | /?     |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * | ESC    |   a  |   o  |   e  |   u  |   i  |------|           |------|   d  |   h  |   t  |   n  |   s  | BSPC   |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * | LShift |   ;: |   q  |   j  |   k  |   x  |   -_ |           |   \| |   b  |   m  |   w  |   v  |   z  | RShift |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   | CTRL | ALT  |      | GUI  |SYM   |                                       |BRA   | GUI  |      | ALT  | CTRL |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        | Home | End  |       | CAPS |        |
 *                                 ,------|------|------|       |------+--------+------.
 *                                 |      |FUN   |PgUp  |       |Insert|        |      |
 *                                 |      |layer |------|       |------|        |      |
 *                                 |Space |      |PgDown|       |Delete|        |Enter |
 *                                 `--------------------'       `----------------------'
 */
[DVORAK] = LAYOUT_ergodox_pretty(
  DV_GRV , DV_1   , DV_2   , DV_3   , DV_4   , DV_5   , DV_LBRC,     DV_RBRC, DV_6   , DV_7   , DV_8   , DV_9   , DV_0   , DV_EQL ,
  KC_TAB , DV_QUOT, DV_COMM, DV_DOT , DV_P   , DV_Y   , DV_SLSH,     DV_EQL , DV_F   , DV_G   , DV_C   , DV_R   , DV_L   , DV_SLSH,
  KC_CAPS, DV_A   , DV_O   , DV_E   , DV_U   , DV_I   ,                       DV_D   , DV_H   , DV_T   , DV_N   , DV_S   , KC_BSPC,
  KC_LSFT, DV_SCLN, DV_Q   , DV_J   , DV_K   , DV_X   , DV_MINS,     DV_BSLS, DV_B   , DV_M   , DV_W   , DV_V   , DV_Z   , KC_RSFT,
  KC_LCTL, KC_LALT, KC_NO  , KC_LGUI, MO(SYM),                                         MO(BRA), KC_RGUI, KC_NO  , KC_RALT, KC_RCTL,

                                               KC_HOME, KC_END ,     KC_ESC , KC_NO  ,
                                                        KC_PGUP,     KC_INS ,
                                      KC_SPC , MO(FUN), KC_PGDN,     KC_DEL , KC_NO  , KC_ENT
),
/* Keymap 1: Symbol keys
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |        | !    | @    | #    | $    | %    |      |           |      | ^    | &    | *    | (    | )    | =      |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |        | '    | ,    | .    |      |      |      |           |      |      |      | COLON|      |      | /      |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        | ADD  | OR   | EQUAL| UNDSC| INTER|------|           |------| DBLQT| HACK | TILDE|NEGATE| SLASH|        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        | ;    | QUOTE|      |      |      |      |           |      |      | MINUS|      |      |      |        |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |      |      |      |      |      |                                       |      |      |      |      |      |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      |      |       |      |      |
 *                                 ,------|------|------|       |------+------+------.
 *                                 |      |      |      |       |      |      |      |
 *                                 |      |      |------|       |------|      |      |
 *                                 |      |      |      |       |      |      |      |
 *                                 `--------------------'       `--------------------'
 */
[SYM] = LAYOUT_ergodox_pretty(
  KC_TRNS, DV_EXLM, DV_AT  , DV_HASH, DV_DLR , DV_PERC, KC_TRNS,     KC_TRNS, DV_CIRC, DV_AMPR, DV_ASTR, DV_LPRN, DV_RPRN, KC_TRNS,
  KC_TRNS, DV_QUOT, DV_COMM, DV_DOT , KC_TRNS, KC_TRNS, KC_TRNS,     KC_TRNS, KC_TRNS, KC_TRNS, DV_COLN, KC_TRNS, KC_TRNS, KC_TRNS,
  KC_TRNS, DV_PLUS, DV_PIPE, DV_EQL , DV_UNDS, DV_QUES,                       DV_DQUO, DV_BSLS, DV_TILD, DV_MINS, DV_SLSH, KC_TRNS,
  KC_TRNS, DV_SCLN, DV_QUOT, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,     KC_TRNS, KC_TRNS, DV_MINS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,                                         KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,

                                               KC_TRNS, KC_TRNS,     KC_TRNS, KC_TRNS,
                                                        KC_TRNS,     KC_TRNS,
                                      KC_TRNS, KC_TRNS, KC_TRNS,     KC_TRNS, KC_TRNS, KC_TRNS
),
/* Keymap 2: Bracket keys
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        | [    | ]    | {    | }    |      |------|           |------|      | (    | )    | <    | >    |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |      |      |      |      |      |                                       |      |      |      |      |      |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      |      |       |      |      |
 *                                 ,------|------|------|       |------+------+------.
 *                                 |      |      |      |       |      |      |      |
 *                                 |      |      |------|       |------|      |      |
 *                                 |      |      |      |       |      |      |      |
 *                                 `--------------------'       `--------------------'
 */
[BRA] = LAYOUT_ergodox_pretty(
  // left hand
  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,     KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,     KC_TRNS, KC_TRNS, DV_LBRC, DV_RBRC, KC_TRNS, KC_TRNS, KC_TRNS,
  KC_TRNS, DV_LBRC, DV_RBRC, DV_LCBR, DV_RCBR, KC_TRNS,                       KC_TRNS, DV_LPRN, DV_RPRN, DV_LABK, DV_RABK, KC_TRNS,
  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,     KC_TRNS, KC_TRNS, DV_LCBR, DV_RCBR, KC_TRNS, KC_TRNS, KC_TRNS,
  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,                                         KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,

                                               KC_TRNS, KC_TRNS,     KC_TRNS, KC_TRNS,
                                                        KC_TRNS,     KC_TRNS,
                                      KC_TRNS, KC_TRNS, KC_TRNS,     KC_TRNS, KC_TRNS, KC_TRNS
),
/* Keymap 4: Function keys, navigation keys, insert, delete
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |        | F1   | F2   | F3   | F4   | F5   |      |           |      | G6   | F7   | F8   | F9   | F10  |        |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |        | F11  | F12  |      |      |      |      |           |      |      |      |      |      |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        | HOME | PGDN | PDUP | END  |INSERT|------|           |------|DELETE| LEFT | DOWN | UP   | RIGHT|        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |      |      |      |      |      |                                       |      |      |      |      |      |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      |      |       |      |      |
 *                                 ,------|------|------|       |------+------+------.
 *                                 |      |      |      |       |      |      |      |
 *                                 |      |      |------|       |------|      |      |
 *                                 |      |      |      |       |      |      |      |
 *                                 `--------------------'       `--------------------'
*/
[FUN] = LAYOUT_ergodox_pretty(
  // left hand
  KC_TRNS, KC_F1  , KC_F2  , KC_F3  , KC_F4  , KC_F5  , KC_TRNS,     KC_TRNS, KC_F6  , KC_F7  , KC_F8  , KC_F9  , KC_F10 , KC_TRNS,
  KC_TRNS, KC_F11 , KC_F12 , KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,     KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
  KC_TRNS, KC_HOME, KC_PGDN, KC_PGUP, KC_END , KC_INS ,                       KC_DEL , KC_LEFT, KC_DOWN, KC_UP  , KC_RGHT, KC_TRNS,
  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,     KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,                                         KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,

                                               KC_TRNS, KC_TRNS,     KC_TRNS, KC_TRNS,
                                                        KC_TRNS,     KC_TRNS,
                                      KC_TRNS, KC_TRNS, KC_TRNS,     KC_TRNS, KC_TRNS, KC_TRNS
),
/* Keymap 5:
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |      |      |      |      |      |------|           |------|      |      |      |      |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |      |      |      |      |      |                                       |      |      |      |      |      |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      |      |       |      |      |
 *                                 ,------|------|------|       |------+------+------.
 *                                 |      |      |      |       |      |      |      |
 *                                 |      |      |------|       |------|      |      |
 *                                 |      |      |      |       |      |      |      |
 *                                 `--------------------'       `--------------------'
[NONAME] = LAYOUT_ergodox_pretty(
  // left hand
  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,     KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,     KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,                       KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,     KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,                                         KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,

                                               KC_TRNS, KC_TRNS,     KC_TRNS, KC_TRNS,
                                                        KC_TRNS,     KC_TRNS,
                                      KC_TRNS, KC_TRNS, KC_TRNS,     KC_TRNS, KC_TRNS, KC_TRNS
),
*/
// !!! COMMA ABOVE, WE NEED MORE
};


layer_state_t layer_state_set_user(layer_state_t state) {

  uint8_t layer = biton32(state);

  ergodox_board_led_off();
  ergodox_right_led_1_off();
  ergodox_right_led_2_off();
  ergodox_right_led_3_off();
  switch (layer) {
    case SYM:
      ergodox_right_led_1_on();
      break;
    case BRA:
      ergodox_right_led_2_on();
      break;
    case FUN:
      ergodox_right_led_3_on();
      break;
    default:
      break;
  }
  return state;
};
